---
title: 黑苹果安装指南之基础
date: 2019-08-22 13:07:44
categories: 
- 黑苹果
tags:
- hackintosh
- 黑苹果
---
## EFI可扩展固件接口（Extensible Firmware Interface）
英特尔公司推出的一种在未来的类PC的电脑系统中替代BIOS的升级方案

## UEFI 统一可扩展固件接口”(Unified Extensible Firmware Interface)是EFI标准化名称
这种接口用于操作系统自动从预启动的操作环境，加载到一种操作系统上。
** UEFI是由EFI1.10为基础发展起来的，它的所有者已不再是Intel，而是一个称作Unified EFI Form的国际组织。 **从uefi启动的硬盘必须是GPT格式。
Launch CSM就是开启CSM这个选项，使原本不完全支持UEFI的系统也能兼容支持UEFI的模块了。
## ESP分区(EFI system partition)
ESP虽然 是一个 FAT16 或 FAT32 格式的物理分区，但是其分区标识是 EF (十六进制) 而非常规的 0E 或 0C。

## MBR 主引导记录（MBR，Master Boot Record）
mbr分区最多支持2.2T的磁盘,并且只支持4个主分区。而GPT则没有这些限制。

## GPT(GUID Partition Table，缩写：GPT)全局唯一标识磁盘分区表
GUID磁盘分区表（GUID Partition Table，缩写：GPT）其含义为“全局唯一标识磁盘分区表”，是一个实体硬盘的分区表的结构布局的标准。不支持UEFI机器是无法使用GPT分区的。