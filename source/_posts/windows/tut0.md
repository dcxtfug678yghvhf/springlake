---
title: windows下操作ESP（EFI分区）
date: 2019-08-22 21:10:14
categories: 
- windows
tags:
- ESP
- EFI分区
---
## DISKPART 操作EFI分区
1. 首先以管理员权限打开cmd
2. 输入diskpart  进入磁盘操作指令
3. list disk 查看所有硬盘的情况
4. sel disk number 选择相应的要进行操作的硬盘
    sel disk 2
5. list par 列出该硬盘所有的分区
6. sel par 4 选择相应的分区
7. 设置为基本数据分区
    set id=ebd0a0a2-b9e5-4433-87c0-68b6b72699c7
8. 挂载到x盘符 assign letter=x，如果删除盘符则执行：remove letter=x
然后进行操作就可以，如果要还原为EFI分区则执行下面指令：set id=C12A7328-F81F-11D2-BA4B-00A0C93EC93B
{% asset_img esp-op.png [windows操作EFI分区] %}


## Windows 下分区表类型以及类型标识(GUID)        
分区类型 | 分区类型 GUID | 
  - | - 
EFI 系统分区 |  c12a7328-f81f-11d2-ba4b-00a0c93ec93b |
基本数据分区 |  ebd0a0a2-b9e5-4433-87c0-68b6b72699c7 |
 Microsoft 保留分区 |  e3c9e316-0b5c-4db8-817d-f92df00215ae |
动态磁盘上的 LDM 元数据分区 |  5808c8aa-7e8f-42e0-85d2-e1e90434cfb3 |
群集元数据分区 |  db97dba9-0840-4bae-97f0-ffb9a327c7e1 |

## Windows特殊的分区
 分区 | 需求 | 描述 |
 - | - | -
 ESP | 必须 | EFI 系统分区 (ESP)，存放系统文件以供引导启动 |
 MSR	Windows | 必须	| Windows 系统特有分区，包含与其他系统分区有关的信息，供 Microsoft 应用程序使用。 |
 恢复 | 可选 | 可选分区，用于存放系统恢复、加密保护工具等。 |

 大家可以输入：**diskmgmt.msc** 命令查看磁盘分区情况。
{% asset_img diskmgmt.png [diskmgmt.msc管理界面] %}
 