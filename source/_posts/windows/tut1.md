---
title: windows 10中如何将MBR分区表转成GPT，并使用UEFI方式启动
date: 2019-08-23 08:47:23
categories:
- windows
tags:
- MBR转GPT
- BIOS转UEFI
---
对于已经安装了的 Windows 10 ，我们想要将引导方式从传统的BIOS引导改为UEFI方式要怎么操作呢？现在跟着 **长沙刘哥** 一起操作吧。

## 首先确认你的引导方式是 BIOS 还是 UEFI ,如果是 UEFI 就不要进行操作了
在运行(win+x)中输入： **msinfo32**
{% asset_img UEFI.png [msinfo32界面，确认引导方式] %}
如果 BIOS 方式一栏是“传统”则表示是 BIOS 引导，如果是 UEFI ，那恭喜你什么都不用操作

## 校验引导盘是否能够转换，注意操作是切换到系统盘。
在运行中输入：**mbr2gpt /validate /allowFullOS**
如果出现错误则要进行排查：
```
C:\Windows\System32>mbr2gpt /validate /allowFullOS
MBR2GPT: Attempting to validate disk 2
MBR2GPT: Retrieving layout of disk
MBR2GPT: Validating layout, disk sector size is: 512 bytes
Disk layout validation failed for disk 2
```
比如我的就出现了： **Disk layout validation failed for disk 2** 错误，这时你要打Windows目录，然后找到这两个日志文件：setupact.log,setuperr.log
{% asset_img log.png [setuperr.log setupact.log] %}
打开并检查具体报错原因，一般的原因都是系统盘没有空余空间来创建EFI分区，这时你需要压缩一部分空间。

## 转换
输入如下命令： mbr2gpt /convert /allowFullOS
