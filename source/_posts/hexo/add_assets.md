---
title: 在hexo中添加图片或文件资源
date: 2019-08-19 13:54:51
tags:
- hexo
- hexo插入图片
categories:
- hexo
---
## 要在hexo中添加图片或者文件链接资源需要进行如下设置，首先要修改_config.yml文件：
```yaml
post_asset_folder: true #将post_asset_folder的值改为true
```
## 新建页面时会自动创建一个资源目录
```bash
hexo new --path hexo/add_assets "在hexo中添加图片或文件资源"
```
执行上面指令之后，会在hexo目录下面生存一个名为add_assets的目录，然后我们将图片资源放入这个目录下面:
{% asset_img 2.png [图片资源目录] %}
## 在文章中引用图片资源
{% blockquote %}
{% raw %}
{% asset_img 2.png [图片的文本点位字符] %} 这条指令是插入图片到文档中
{% asset_path 2.png %} 这条指令是引用路径
{% asset_link 2.png %} 这条指令是链接到图片
{% endraw %}
{% endblockquote %}

## 使用MarkDown标签插入图片
首先在source目录中创建一个文件夹，比如images。然后把图片放入images目录中。这时我们可以使用
{% raw %}
![](/images/2.png) 标签就可以了。
{% endraw %}
