---
title: 使用 hexo 和 gitee pages 服务搭建个人 blog（2019版）
date: 2019-08-19 09:59:31
tags:
categories:
- hexo
---
hexo 是基于 nodejs 开发的一个静态站点生成工具。它可以把使用 markdown、asciidoc……等文档格式来撰写文章转化成 html 格式，然后可以将这些html文档上传到任何支持html的静态空间比如（gitee pages、github pages）。

## 最近尝试将blog迁移到gitee pages上面。我使用gitee原因有好几个：
1. 免费使用，大小为1G，这个大小可以写非常多的文章了
2. 可以使用 git 进行版本管理
3. 方便其它的静态站点生成工具比如：hexo, hugo……

## 安装 hexo
- 安装 nodejs，下载地址: [https://nodejs.org/en/download/](https://nodejs.org/en/download/)
- 安装 git，下载地址：[https://git-scm.com/download/win](https://git-scm.com/download/win)
    安装完这两个工具之后，我们需要对 nodejs 进行一下设置,这个是一个bat文件，可以把这一段内容保存为一个xx.bat，然后双击运行即可自动完成设置。
```bash
REM npm 镜像设置
npm i -g mirror-config-china --registry=https://registry.npm.taobao.org
npm i -g yarn

REM 修改node配置目录
md D:\tools\node-modules\modules D:\tools\node-modules\caches
npm config set prefix "D:\tools\node-modules\modules"
npm config set cache "D:\tools\node-modules\caches"

REM 在环境变量中增加
setx PATH "%PATH%;D:\tools\node-modules\modules" /M
```
- 安装 hexo
```bash
npm i -g hexo-cli
```

## 使用hexo创建blog
* 初始化站点
```bash
hexo init <目录>
```
* 创建页面
```bash
hexo new --path hexo/hello "测试页面"
```
    上面这一行的意思是：在_source目录下面创建一个hexo/hello.md的页面，并在设置它的标题为“测试页面”
* 测试站点
```bash
hexo server
```

## 注册 gitee 服务
大家可以打开这个网址：[https://gitee.com/signup](https://gitee.com/signup)完成注册，注册完成之后需要创建一个仓库。这个仓库要和你的用户名同名，这样以便可以三级域名方式进行访问，不用带目录。比如我的用户名是 linvers，这时我创建一个linvers的仓库，这时我可以使用[linvers.gitee.io](https://linvers.gitee.io)进行访问。

## 发布 blog 到gitee pages上面
在完成注册gitee之后需要，需要进行如下操作，首先要在blog安装一个插件：hexo-deployer-git，请使用下面指令进行安装，这个操作要在当前blog的目录下进行：
```bash
npm i -S hexo-deployer-git
```
安装完成之后需要在修改_config.yml文件在里面增加如下代码：
```yaml
 deploy:
  type: git
  repo: git@gitee.com:linvers/linvers.git #这里修改为你的仓库地址
  branch: master
```
发布：
```bash
hexo generate --deploy
```
## 更新并生效
部署到gitee pages上面之后并不会立即生效，这时你要更新一下发布才会生效

{% asset_img gitee.png [点击“更新”按钮重新部署blog] %}
